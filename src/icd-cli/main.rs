//! The ICD commandline application
//!
//! This ICD binary is used to communicate with the server that interfaces with
//! camera hardware to capture images, and perform time lapse photograpy.

#[macro_use]
extern crate log;
extern crate env_logger;
extern crate base64;
extern crate chrono;
#[macro_use]
extern crate clap;
#[macro_use]
extern crate serde_derive;
extern crate serde_yaml;
extern crate reqwest;

mod config;
mod types;
//mod camera;
mod image;
//mod job;

use clap::{App, Shell};
use std::io;
use std::process;

use config::Config;

fn main() {
    env_logger::init();

    match run() {
        Ok(_) => {}
        Err(e) => {
            eprintln!("error: {}", e);
            process::exit(1);
        }
    }
}

fn run() -> Result<(), Box<::std::error::Error>> {
    let yaml = load_yaml!("cli.yaml");
    let matches = App::from_yaml(yaml).get_matches();
    // TODO: Figure out out to use this instead of RUST_LOG, and remove _
    let _verbose = matches.is_present("verbose");

    let fname = matches.value_of("config").unwrap_or("icd.yml");
    let mut config = Config::new();
    config.load(&fname).unwrap();

    // TODO: Need to check here for either config + name, or address info
    let name = matches.value_of("name").unwrap_or("localhost");
    let idx = config.servers
        .iter()
        .position(|s| s.name == name)
        .unwrap();
    let server = &config.servers[idx];

    match matches.subcommand() {
        ("camera", Some(_)) => camera(),
        ("image", Some(c)) => match c.subcommand() {
            ("capture", Some(m)) => image::capture(m)?,
            ("delete", Some(m)) => image::delete(&server, m)?,
            ("download", Some(m)) => image::download(&server, m)?,
            ("list", Some(m)) => image::list(&server, m)?,
            (_, _) => unreachable!(),
        },
        ("job", Some(_)) => job(),
        ("completions", Some(c)) => {
            if let Some(shell) = c.value_of("shell") {
                App::from_yaml(yaml).gen_completions_to(
                    "icdc",
                    shell.parse::<Shell>().unwrap(),
                    &mut io::stdout(),
                );
            }
        }
        (_, _) => unreachable!(),
    }

    Ok(())
}

fn camera() {
    println!("camera");
}

fn job() {
    println!("job");
}
