#[derive(Debug, Serialize, Deserialize)]
pub struct Server {
    pub name: String,
    pub version: u32,
    pub address: String,
    pub port: u32
}
