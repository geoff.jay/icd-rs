extern crate reqwest;
extern crate serde_json;

use std::str;
use std::fs::File;
use std::error::Error;
use std::io::prelude::*;
use base64::decode;
use chrono::prelude::*;
use chrono::{Utc, TimeZone};
use clap::ArgMatches;
use reqwest::{Url};

use types::*;

#[derive(Serialize, Deserialize, Debug)]
struct Image {
    id: u16,
    name: String,
    timestamp: i64,
    width: Option<u16>,
    height: Option<u16>,
    data: Option<String>,
}

type Images = Vec<Image>;

pub fn capture(_m: &ArgMatches) -> Result<(), Box<Error>> {
    debug!("image::capture");

    Ok(())
}

pub fn delete(s: &Server, m: &ArgMatches) -> Result<(), Box<Error>> {
    debug!("image::delete");

    let from = value_t!(m.value_of("from"), u32).unwrap();
    let to = value_t!(m.value_of("to"), u32).unwrap();

    for n in from..=to {
        let url: Url = format!("http://{}:{}/api/images/{}", s.address, s.port, n)
            .parse()
            .unwrap();

        let client = reqwest::Client::new();
        let resp = client.delete(url)
            .send()?;

        info!("{}", resp.status());
        for header in resp.headers().iter() {
            info!("{}: {}", header.name(), header.value_string());
        }
    }

    Ok(())
}

// NOTE: This is currently just setup to be run as, eg.
// icdc -c icd.yml -n icd01 image download --from 1 --to 100 --offset 6
//
// This will download the first 100 images using the Alberta timezone,
// which is what is needed for now.
pub fn download(s: &Server, m: &ArgMatches) -> Result<(), Box<Error>> {
    debug!("image::download");

    let from = value_t!(m.value_of("from"), u32).unwrap();
    let to = value_t!(m.value_of("to"), u32).unwrap();
    let offset = value_t!(m.value_of("offset"), i32).unwrap();

    for n in from..=to {
        let url: Url = format!("http://{}:{}/api/images/{}/all",
                               s.address,
                               s.port,
                               n)
            .parse()
            .unwrap();

        let mut resp = reqwest::get(url).expect("Failed to send request");
        info!("{}", resp.status());
        for header in resp.headers().iter() {
            info!("{}: {}", header.name(), header.value_string());
        }

        let mut buf = String::new();
        resp.read_to_string(&mut buf).expect("Failed to read response");
        let image = serde_json::from_str::<Image>(buf.as_str()).unwrap();

        let dt = Utc.timestamp(image.timestamp, 0);
        let mut dt_offset: FixedOffset;
        if offset < 0 {
            dt_offset = FixedOffset::east(offset * 3600 * -1);
        } else {
            dt_offset = FixedOffset::west(offset * 3600);
        }
        let utc = dt.with_timezone(&dt_offset);
        let fname = format!("{}-{}.jpg",
                            s.name,
                            utc.format(&"%Y-%m-%d_%H-%M-%S".to_string()));
        let mut file = File::create(fname)?;
        let data = &Some(image.data)
            .unwrap()
            .unwrap();
        file.write_all(&decode(&data.clone().into_bytes()).unwrap()[..])?;
    }

    Ok(())
}

pub fn list(s: &Server, _m: &ArgMatches) -> Result<(), Box<Error>> {
    debug!("image::list");

    let url: Url = format!("http://{}:{}/api/images", s.address, s.port)
        .parse()
        .unwrap();

    let mut resp = reqwest::get(url).expect("Failed to send request");
    info!("{}", resp.status());
    for header in resp.headers().iter() {
        info!("{}: {}", header.name(), header.value_string());
    }

    let mut buf = String::new();
    resp.read_to_string(&mut buf).expect("Failed to read response");
    let images = serde_json::from_str::<Images>(buf.as_str()).unwrap();
    info!("Response contains {} images", &images.len());
    for image in &images {
        info!("{:?}", image);
    }

    Ok(())
}
