extern crate gphoto;

use std::sync::mpsc::channel;
use std::thread;
use std::time::Duration;
use std::path::Path;

fn main() {
    let mut context = gphoto::Context::new().unwrap();
    let mut camera = gphoto::Camera::autodetect(&mut context).unwrap();
    let (tx, rx) = channel();

    thread::spawn(move || {
        loop {
            thread::sleep(Duration::from_secs(3));
            tx.send("capture").unwrap();
        }
    });

    loop {
        let _ = rx.try_recv().map(|reply| {
            println!("{}", reply);
            let capture = camera.capture_image(&mut context).unwrap();
            let mut file = gphoto::FileMedia::create(Path::new(&*capture.basename())).unwrap();
            camera.download(&mut context, &capture, &mut file).unwrap();
        });
    }
}
